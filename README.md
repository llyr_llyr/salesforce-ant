<a name="intro"></a>
## Intro

<a name="goals"></a>
### Description
Project that contains a Docker image that will allow Salesforce CI to run. 

### Steps to push to hub.docker.com
- `docker build -t salesforce-ant .` - builds the image
- `docker run -it salesforce-ant /bin/bash` - test
- `docker tag salesforce-ant llyrjones/salesforce-ant` - tag the image
- `docker push llyrjones/salesforce-ant:latest`  - push the image

